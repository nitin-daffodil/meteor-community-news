Template.newsList.onRendered(function(template) {
  var self = this;
  self.filter = new ReactiveTable.Filter('date-filter', ['created_at']);      
    if (Meteor.user() != undefined && Meteor.user().roles != undefined && Meteor.user().roles.length !== 0 && !Session.get("optionSelectedForNews")) {
        Session.set("optionSelectedForNews", "googleNews");
    }
    self.autorun(function () {  
      var toDate = Session.get("DateSesionForNewsList");
      var fromDate = Session.get("DateSesionForNewsList");
      toDate.setHours(23,59,59,999);
      fromDate.setHours(0,0,0,0);
      console.log("Todate",toDate);
      console.log("fromdate",fromDate);
      var filter = {
          '$gt': Math.round(fromDate.getTime() / 1000),
          '$lt': Math.round(toDate.getTime() / 1000)
        }
      console.log("Filter",filter);
      self.filter.set(filter);
    });    
  this.$('.datetimepicker').datetimepicker({
    format: 'MM-DD-YYYY'
  }).on('dp.change', function(e, date) {
      Session.set("DateSesionForNewsList",e.date._d);      
  });;
});
Template.newsList.onCreated(function() {
    var newsListInstance = this;
    newsListInstance.autorun(function() {
        var parameters = {};
        var dateRangeGiven;
        if (Meteor.user() != undefined) {
            if (Meteor.user().roles != undefined && Meteor.user().roles.length !== 0 && Session.get("optionSelectedForNews")) {
                parameters = Session.get("optionSelectedForNews");
            } else {
                parameters = Meteor.user()._id;
                dateRangeGiven = false;
            }
        }
        Meteor.subscribe('listOfNews', parameters)
    });
});
Template.newsList.helpers({
    'showDatePicker': function() {
        return Session.get("dateRangeFilter");
    },
    listOfNews: function() {
        return News.find({}).fetch()
    },
    settings: function() {
        return {
            showFilter: true,
            fields: [{
                key: 'title',
                label: 'Title'
            }, {
                key: 'address',
                label: 'News For'
            }, {
                key: 'userName',
                label: 'Published By',
                sortable: false
            }, {
                key: 'created_at',
                label: 'PublicationDate',
                sortByValue: true,
                fn: function(value) {
                    var date = new Date(value * 1000)
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month + '/' + day + '/' + year;
                }
            }, {
                key: 'edit',
                label: 'Edit',
                sortable: false,
                fn: function(value) {
                    return new Spacebars.SafeString('<button class="editNews">Edit</button>');
                }
            }, {
                key: 'Delete',
                label: 'Delete',
                sortable: false,
                fn: function(value) {
                    return new Spacebars.SafeString('<button class="deleteNews">Delete</button>');
                }
            }],
            showNavigation: 'auto',
            showColumnToggles: true,
            showFilter: false,
            class: "table table-striped table-hover table-bordered ui sortable ",
            filters: ['dateFilter','date-filter']
        };
    },
    initialDate:function(){
      var dateObj = new Date();
      if (!Session.get("DateSesionForNewsList")) {
        Session.set("DateSesionForNewsList", dateObj);
      }
      return Session.get("DateSesionForNewsList").getMonth() + 1 + "-" + Session.get("DateSesionForNewsList").getDate() + "-" + Session.get("DateSesionForNewsList").getFullYear();
    },    
    selectOptions: function() {
        return [{
            'text': 'Google News',
            'value': 'googleNews'
        }, {
            'text': 'Others',
            'value': "otherNews"
        }, {
            'text': 'All News',
            'value': "allNews"
        }, {
            'text': 'Today',
            'value': 'today'
        }, {
            'text': 'Last 7 Days',
            'value': '7days'
        }, {
            'text': 'Last 30 Days',
            'value': '30days'
        }];
    },
    selectedIfEquals: function(value) {
        return value == Session.get("optionSelectedForNews") ? "selected" : "";
    },
    showSelectList: function() {
        if (Meteor.user()) {
            if (Meteor.user().roles != undefined && Meteor.user().roles.length !== 0)
                return true;
            else
                return false;
        }
    }
});
Template.newsList.events({
    "click .reactive-table tbody tr": function(template, e) {
        if (event.target.className == "editNews") {
            Router.go('newsEdit', {
                '_id': this._id
            })
        }
        if (event.target.className == "deleteNews") {
            console.log("Within Delete");
            Meteor.call("deleteNews", this._id, function(err) {
                if (err) {
                    console.log("Error", err.error);
                }
            })
        }
    },
    'change select': function(e, template) {
        e.preventDefault();
        var daysSlected = template.find('#news').value;
        Session.set("optionSelectedForNews", daysSlected);
    }
})
