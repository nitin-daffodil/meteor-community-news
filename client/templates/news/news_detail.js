Template.NewsDetail.helpers({
  newsDetail: function() {
    console.log("Session:",Session.get("dateRangeSelected"));
    return this.newsDetail[0];
  },
  formatedDate: function(timestamp) {
    var date = new Date(timestamp * 1000)
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    return month + '/' + day + '/' + year;
  }
})
