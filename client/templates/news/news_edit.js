Template.newsEdit.helpers({
  newsToBeEdit: function() {
    return this.newsToBeEdit[0];
  },
  dateInFormat: function (timestamp) {
    var dateObj = new Date(timestamp * 1000);
    var month = dateObj.getMonth() + 1; //months from 1-12
    var day = dateObj.getDate();
    var year = dateObj.getFullYear();
    return month + "-" + day + "-" + year;  
  }
});
Template.newsEdit.events({
  "click #editNews": function (e, t) {
    e.preventDefault();
    var title = t.find('#titleOfEditNews').value;
    var description = t.find('#descriptionOfEditNews').value;
    var address = t.find('#addressOfEditNews').value;
    var date = new Date(t.find('#dateOfEditNews').value);
    newdate = new Date();
    newdate.setDate(date.getDate());
    newdate.setMonth(date.getMonth());
    newdate.setFullYear(date.getFullYear());
    var image = t.find('#imageOfEditNews').src;
    Session.set("loading", true);
    Meteor.call("updateNews", title, description, address, image, newdate, Router.current().params._id, function(err) {
      if (err) {
        console.log("Error " , err.error);
        Session.set("loading", false);
      } else {
        Router.go('newsList');
        Session.set("loading", false);
      }
    })
  }
});
Template.newsEdit.onRendered(function() {
  this.$('.datetimepicker').datetimepicker({
    format: 'MM-DD-YYYY'
  }).on('dp.change #dateOfEditNews', function(e, date) {})
});
Template.fileUploadEdit.rendered = function() {
  uploadImageOnCloudinary('imageOfEditNews');
};