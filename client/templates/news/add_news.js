Template.addNews.helpers({
  todayDate: function() {
    var dateObj = new Date();
    var month = dateObj.getMonth() + 1; //months from 1-12
    var day = dateObj.getDate();
    var year = dateObj.getFullYear();
    newdate = month + "-" + day + "-" + year;
    return newdate;
  }  
})
Template.addNews.events({
  "click #saveNews": function (e, t) {
    e.preventDefault();
    var title = t.find('#title').value;
    var description = t.find('#description').value;
    var address = t.find('#address').value;
    var date = new Date(t.find('#date').value);
    newdate = new Date();
    newdate.setDate(date.getDate());
    newdate.setMonth(date.getMonth());
    newdate.setFullYear(date.getFullYear());
    var image = t.find('#newsImage').src;
    Session.set("loading", true);
    Meteor.call("saveNews", title, description, address, image, newdate, function (err) {
      if (err) {
          console.log("Error", err.error);
          Session.set("loading", false);
      } else {
          Session.set("loading", false);
          Router.go('newsList');
      }
    })
  }  
})
Template.addNews.onRendered(function() {
  this.$('.datetimepicker').datetimepicker({
      format: 'MM-DD-YYYY'
  }).on('dp.change', function (e, date) {

  })
});
Template.fileUpload.rendered = function() {
   uploadImageOnCloudinary('newsImage');
};


