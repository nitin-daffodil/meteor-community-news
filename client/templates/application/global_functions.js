Meteor.startup(function() {
  GoogleMaps.load();
  cloudinaryUploadPreset = 'jzf0zwjy'; // Set your Upload Preset here
  cloudinaryCloudName = 'dumvd6dw3'; // Set your Cloud Name here  
});
// Here, we use Cloudinary CDN service for hosting images.
// Required `Upload Preset` and `Cloud Name` for uploading images on Cloudinary.
// More detail can be found here : http://cloudinary.com/blog/direct_upload_made_easy_from_browser_or_mobile_app_to_the_cloud
// generic function to upload image on Cloudinary 
uploadImageOnCloudinary = function(imageId) {
  $('input').unsigned_cloudinary_upload(cloudinaryUploadPreset, {
    cloud_name: cloudinaryCloudName,
    tags: 'browser_uploads'
  }).bind('cloudinarydone', function(e, data) {
    document.getElementById(imageId).src = data.result.secure_url;
    Session.set("loading", false);
  }).bind('cloudinaryprogress', function(e, data) {
    console.log("data loaded is : " + data.loaded + " data size: " + data.total);
    Session.set("loading", true);
  });
}