UI.registerHelper("pageTitle", function() {
  var title;
  title = Router.current().route.options.title
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, function(str) {
      return str.toUpperCase();
    });
  document.title = "CNews | " + title;
});
UI.registerHelper("homePage", function() {
  var title;
  title = Router.current().route.options.title;
  if (title === 'Map')
    return 'home';
  else
    return 'page';
});