Blaze._allowJavascriptUrls(); //to allow the action="javascript:" in the <form> element
Template.header.helpers({
  activeIfTemplateIs: function (template) {
      var currentRoute = Router.current();
      return currentRoute &&
        template.toLowerCase() === currentRoute.lookupTemplate().toLowerCase() ? 'active' : '';
  }
});

