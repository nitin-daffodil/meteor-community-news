Template.map.onRendered(function() {
    if (!Session.get("dateRangeSelected")) {
        Session.set("dateRangeSelected", "7days");
    }
    if (!Session.get("dateRange")) {
        Session.set("dateRange", false);
    }
});
Template.map.onCreated(function() {
    var mapInstance = this;
    mapInstance.autorun(function() {
        var sessionSet = {};
        var dateRangeGiven;
        if (Session.get("dateRange") && Session.get("toDateNews") && Session.get("fromDateNews")) {
            dateRangeGiven = true;
            sessionSet.toDateFromSession = Session.get("toDateNews");
            sessionSet.fromDateFromSession = Session.get("fromDateNews");
        } else if (Session.get("dateRangeSelected")) {
            dateRangeGiven = false;
            sessionSet.daySelected = Session.get("dateRangeSelected");
        }
        Meteor.subscribe('newsByDateFilter', dateRangeGiven, sessionSet)
    });
    var markers = [];
    GoogleMaps.ready('map', function(map) {
        if (typeof OverlappingMarkerSpiderfier !== 'undefined') {
            mapFunction(map, markers);
        } else {
            Meteor.Loader.loadJs("/files/oms.min.js", function() {
                mapFunction(map, markers);
            });
        }

    });
});
Template.home.helpers({
    selectOptions: function() {
        return [{
            'text': 'All News',
            'value': 'all'
        }, {
            'text': 'Last 7 Days',
            'value': "7days"
        }, {
            'text': 'Last 30 Days',
            'value': "30days"
        }, {
            'text': "Today",
            'value': "today"
        }, {
            'text': 'Between',
            'value': 'between'
        }];
    },
    selectedIfEquals: function(value) {
        return value == Session.get("dateRangeSelected") ? "selected" : "";
    },
    datePicker: function() {
        return Session.get("dateRange");
    }
})
Template.home.events({
    'change select': function(e, template) {
        e.preventDefault();
        var daysSlected = template.find('#days').value;
        if (daysSlected == 'between') {
            Session.set("dateRange", true);
            Session.set("dateRangeSelected", daysSlected);
        } else {
            Session.set("dateRange", false);
            Session.set("dateRangeSelected", daysSlected);
        }
    }
});
Template.map.helpers({
    mapOptions: function() {
        if (GoogleMaps.loaded()) {
            return {
                center: new google.maps.LatLng(29.1491875, 75.7216527),
                zoom: 3
            };
        }
    }
});
// Template.map.onCreated(function() {

// });
Template.datePickerTemplate.helpers({
    toDate: function() {
        var dateObj = new Date();
        if (!Session.get("toDateNews")) {
            Session.set("toDateNews", dateObj);
        }
        return Session.get("toDateNews").getMonth() + 1 + "-" + Session.get("toDateNews").getDate() + "-" + Session.get("toDateNews").getFullYear();
    },
    fromDate: function() {
        var today = new Date();
        var fromDate = new Date(today.setDate(today.getDate() - 30));
        if (!Session.get("fromDateNews")) {
            Session.set("fromDateNews", fromDate);
        }
        return Session.get("fromDateNews").getMonth() + 1 + "-" + Session.get("fromDateNews").getDate() + '-' + Session.get("fromDateNews").getFullYear();
    }
})
Template.datePickerTemplate.onRendered(function() {
    this.$('.datetimepicker').datetimepicker({
        format: 'MM-DD-YYYY'
    }).on('dp.change', function(e, date) {
        if (e.target.id == 'toDate') {
            dateWithTime = new Date();
            dateWithTime.setDate((e.date._d).getDate());
            dateWithTime.setMonth((e.date._d).getMonth());
            dateWithTime.setFullYear((e.date._d).getFullYear());
            Session.set("toDateNews", dateWithTime);
        } else if (e.target.id == 'fromDate') {
            newdateWithTime = new Date();
            newdateWithTime.setDate((e.date._d).getDate());
            newdateWithTime.setMonth((e.date._d).getMonth());
            newdateWithTime.setFullYear((e.date._d).getFullYear());
            Session.set("fromDateNews", newdateWithTime);
        }
    });
});
mapFunction = function(map, markers) {
    var oms = new OverlappingMarkerSpiderfier(map.instance, {
        markersWontMove: true,
        markersWontHide: true,
        keepSpiderfied: true
    });
    var zIndex = 1;
    var infowindow = new google.maps.InfoWindow({
        content: ''
    });
    oms.addListener('click', function(marker, event) {
        infowindow.close();
        infowindow.setContent(marker.contentString);
        infowindow.open(map.instance, marker);
    });
    oms.addListener('spiderfy', function(markers) {
        infowindow.close();
    });
    oms.addListener('unspiderfy', function(markers) {
        infowindow.close();
    });
    var legend = document.getElementById('legend');
    var content = [];
    content.push('<h3>News of</h3>');
    content.push('<p><div class="color red"></div>Today</p>');
    content.push('<p><div class="color yellow"></div>Last Week</p>');
    content.push('<div class="color blue"></div>Others');
    if (legend) {
        legend.innerHTML = content.join('');
    }
    legend.index = 1;
    map.instance.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
    News.find().observe({
        added: function(document) {
            var contentString = '<span></span>';
            var truncatedTitle;
            var truncatedDescription;
            var iconpath = "/img/blue-dot.png";
            var timestampFromDataBase = document.created_at;
            var dateObjFromTimestamp = new Date(timestampFromDataBase * 1000);
            // var todayDateObjectWithTime = new Date();
            // var dd = todayDateObjectWithTime.getDate();
            // var mm = todayDateObjectWithTime.getMonth() + 1; //January is 0!
            // var yyyy = todayDateObjectWithTime.getFullYear();
            // var todayDateObjectWithOutTime = mm + '-' + dd + '-' + yyyy;
            // var todayTimestamp = new Date(todayDateObjectWithOutTime).getTime() / 1000;
            // var today = new Date();
            // var dateObjectBefore7days = new Date(today.setDate(today.getDate() - 7));
            // var timestampBefore7days = new Date(dateObjectBefore7days).getTime() / 1000;
            var dateShown = dateObjFromTimestamp.getMonth() + 1 + "/" + dateObjFromTimestamp.getDate() + "/" + dateObjFromTimestamp.getFullYear();
            var todayDate = moment().startOf('day').format('X');
            var timestampBefore7days = moment().subtract(7, 'days').startOf('day').format('X')
            if (timestampFromDataBase > timestampBefore7days && timestampFromDataBase < todayDate) {
                iconpath = "/img/yellow-dot.png";
            } else if (timestampFromDataBase >= todayDate && timestampFromDataBase <= moment().format('X')) {
                iconpath = "/img/red-dot.png";
            }
            var pos = new google.maps.LatLng(document.loc.lat, document.loc.lng);
            if (markers.length != 0) {
                for (i = 0; i < markers.length; i++) {
                    var existingMarker = markers[i];
                    var loc = existingMarker.getPosition();
                    //if a marker already exists in the same position as this marker
                    if (loc.equals(pos)) {
                        //update the position of the coincident marker by applying a small multipler to its coordinates
                        var newLat = loc.lat() + (Math.random() - .5) / 150; // * (Math.random() * (max - min) + min);
                        var newLng = loc.lng() + (Math.random() - .5) / 150; // * (Math.random() * (max - min) + min);
                        pos = new google.maps.LatLng(newLat, newLng);
                    }
                }
            }
            if (iconpath === '/img/red-dot.png') {
                zIndex = 3;
            } else if (iconpath === '/img/yellow-dot.png') {
                zIndex = 2;
            } else if (iconpath === '/img/blue-dot.png') {
                zIndex = 1;
            }
            var marker = new google.maps.Marker({
                draggable: false,
                animation: google.maps.Animation.DROP,
                position: pos,
                map: map.instance,
                labelContent: document.title,
                _id: document._id,
                icon: iconpath,
                zIndex: zIndex
            });
            if (document.title.length > 50) {
                truncatedTitle = document.title.substring(0, 50) + '...';
            } else {
                truncatedTitle = document.title;
            }
            if (document.description.length > 100) {
                truncatedDescription = document.description.substring(0, 100) + '...';
            } else {
                truncatedDescription = document.description;
            }
            var newsUrl = '/news/detail/' + document._id;
            var linkTarget = "_self";
            if (document.news_link) {
                newsUrl = document.news_link;
                linkTarget = "_blank";
            }
            if (document.image != undefined) {
                contentString = '<div class="pull-right img_pad" style="max-width:135px;paddding-left:10px;"><IMG BORDER="0" ALIGN="RIGHT" SRC="' + document.image + '" style="max-width:100%;paddding-left:10px;"></div></div>'
            }
            contentString = contentString + '<div id="content">' + '<div class="removePadding"><h3 class="popup_h3"><a class="newsTitle" href=' + newsUrl + ' style="color:#333;" target=' + linkTarget + '>' + truncatedTitle + '</a></h3>' + '<span class="infowindow">' + document.userName + '</span>' + '<span class="infowindow">' + dateShown + '</span>' + '<div class="des_infowindow">' + truncatedDescription + '  <span class=""><a href=' + newsUrl + '  style="color:black;float:right" target =' + linkTarget + '>read more</a></span></div>' + '</div>' + '</div>' + '</div>';
            // contentString = contentString + '<div id="content">' + '<div class="removePadding"><h3 class="popup_h3"><a target=' + linkTarget + 'class="newsTitle" id="linkForNews" href="{{pathFor "NewsDetail" }}" style="color:#333;">' + truncatedTitle + '</a></h3>' + '<span class="infowindow">' + document.userName + '</span>' + '<span class="infowindow">' + dateShown + '</span>' + '<div class="des_infowindow">' + truncatedDescription + '  <span class=""><a href=' + newsUrl + '  style="color:black;float:right" target =' + linkTarget + '>read more</a></span></div>'  + '</div>' + '</div>' + '</div>';
            marker.contentString = contentString;
            oms.addMarker(marker);
            markers.push(marker);
        },
        changed: function(newDocument, oldDocument) {
            if (markers[0]) {
                markerTochange = markers.filter(function(marker) {
                    if (marker) {
                        return marker._id == oldDocument._id;
                    }
                });
                if (markerTochange[0]) {
                    markerTochange[0].setPosition({
                        lat: newDocument.loc.lat,
                        lng: newDocument.loc.lng
                    });
                }
            }
        },
        removed: function(oldDocument, x) {
            if (markers[0]) {
                markerTochange = markers.filter(function(marker) {
                    if (marker) {
                        return marker._id == oldDocument._id;
                    }
                });
                if (markerTochange[0]) {
                    markerTochange[0].setMap(null);
                    markers.splice(markers.indexOf(markerTochange[0]), 1);
                }
            }
        }
    });
}
