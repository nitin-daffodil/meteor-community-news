App.info({
  name: 'Community News',
  description: 'An Android app built with Meteor',
  version: '0.0.1'
});

App.icons({
  'android_ldpi': 'images/images.jpg',
  'android_mdpi': 'images/images.jpg',
  'android_hdpi': 'images/images.jpg',
  'android_xhdpi': 'images/images.jpg'
});


App.accessRule('*');
App.accessRule('*.google.com/*');
App.accessRule('*.googleapis.com/*');
App.accessRule('*.gstatic.com/*');