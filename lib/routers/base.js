Router.configure({
  loadingTemplate: 'loading',
  layoutTemplate: 'layout',
  notFoundTemplate: 'notFound'
});
Router.route('/', {
  name: 'home',
  title: 'Map' 
});


Router.route('/news/detail/:_id', {
  name: 'NewsDetail',
  title: 'News Detail',
  waitOn: function() {
    console.log("Session: within waiton",Session.get("dateRangeSelected"));
    return [
      Meteor.subscribe('listOfNews'),
    ]
  },
  data: function() {
    console.log("Session: within data",Session.get("dateRangeSelected"));
    return {
      newsDetail: News.find({
        "_id": this.params._id
      }).fetch()
    };
  }
  
});
/*Router.map(function() {
  this.route('serverFile', {
    where: 'server',
    path: /^\/.uploads\/(.*)$/,
    action: function() {
      var fs = Npm.require('fs');
      var filePath = process.env.PWD + '/.uploads/' + this.params;
      var data = fs.readFileSync(filePath);
      this.response.writeHead(200, {
        'Content-Type': 'image'
      });
      this.response.write(data);
      this.response.end();
    }
  });
});*/
Router.route('/news/add', {
  name: 'addNews',
  title: 'Add News',
  onBeforeAction: function() {
    if (!Meteor.user() && !Meteor.loggingIn()) {
      Router.go('home');
    } else {
      // required by Iron to process the route handler
      this.next();
    }
  }
});
Router.route('/news/edit/:_id', {
  name: 'newsEdit',
  title: 'Edit Question',
  onBeforeAction: function() {
    if (!Meteor.user() && !Meteor.loggingIn()) {
      Router.go('home')
    } else {
      this.next();
    }
  },
  waitOn: function() {
    console.log("News Edit");
    return [
      Meteor.subscribe('newsToBeEdit', this.params._id)
    ]
  },
  data: function() {
    return {
      newsToBeEdit: News.find({
        "_id": this.params._id
      }).fetch()
    };
  }
});
Router.route('/news/my-list', {
  name: 'newsList',
  title: 'My News',
  onBeforeAction: function() {
    if (!Meteor.user() && !Meteor.loggingIn()) {
      Router.go('home')
    } else {
      this.next();
    }
  },
  waitOn: function() {
    // console.log("currentUser",Meteor.user());
    // var parameters;
    // if(Meteor.user() != undefined){      
    //   if(Meteor.user().roles != undefined &&Meteor.user().roles.length !== 0){
    //      parameters = Meteor.user().roles[0];
    //   } else { 
    //      parameters = Meteor.user()._id;
    //   }
    // }
    // return [
    //   Meteor.subscribe('listOfNews', parameters)
    // ]
  },
  data: function() {
    // return {
    //   listOfNews: News.find({}).fetch()
    // };
  } 
  // fastRender: true
});
// if(Meteor.isServer){
//   FastRender.onAllRoutes(function(path) {
//     Meteor.subscribe('currentUser');
//   })  
// }