News = new Mongo.Collection("news");
var Schemas = {};
Schemas.New = new SimpleSchema({
  title: {
    type: String
  },
  description: {
    type: String 
  },
  address: {
    type: String 
  },
  created_by: {
    type: SimpleSchema.RegEx.Id
  },
  created_at: {
    type: Number
  },
  loc: {
    type: Object
  },
  "loc.lat": {
        type: Number,
        decimal:true
   },
   "loc.lng": {
        type: Number,
        decimal:true
   },
  userName:{
  	type:String
  },
  image:{
  	type:String,
  	optional: true
  },
  news_link:{
    type:String,
    optional: true
  },
  medium:{
    type:String
  }

});

News.attachSchema(Schemas.New);

/*News.after.insert(function (userId, doc) {
  console.log("UserId",userId);
  console.log("doc",doc);
  console.log("Condition result",Meteor.isClient);
  if(Meteor.isClient){
    console.log("with in isclient");
    toastr.success(doc.title);
  }
});*/
