# README #

This application will be used to represent community news on Map.

### What is this repository for? ###

* This is a sample meteor application.

### Requirements for development ###
* meteor

### Features ###
* Any User can login using Social login and contribute news.
* User can upload images in news and all news are display in Google Map.
* User can edit there own news.
* Map show news in marker and visitor can read complete news by clicking on "Read More" link provide in marker information.
* Map Markers represent news in different color : Red color (Today news) ,Yellow color (Past 7 days news) ,Blue color (Other days news)

### How do I get set up? ###

If you don't have Meteor installed already, from a terminal window:


```
#!shell

curl install.meteor.com | /bin/sh

```
To run the app:

```
#!shell

# from ~/meteor-community-new
meteor
```

The app should now be running on http://localhost:3000.

### For Login into Application ###

First, you'll need to register your app on Facebook, Google+ and Twitter.


For this click on 'Sign in' button you found these options/buttons :


* Configure Facebook Login
* Configure Google Login
* Configure Twitter Login

click on any option/button you want to configure and follow the instructions mention in popup.

### Contribution guidelines ###

* Writing tests
* Code review
* Post issues

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Credit ###

This application was created by [Daffodil Software Ltd.](http://www.daffodilsw.com)