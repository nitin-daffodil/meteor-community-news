Fixtures = {
    create: function() {
        News.insert({
            "title": "Prices of fuel, gold up in Kabul",
            "description": "Following decline in value of Afghani currency against dollar, the prices of necessary food items and gold has edged up during the outgoing week in capital Kabul.\n\nMoneychanger Ahmad Shah said buying rate of one US dollar was 60,50afs, while 1,000 Pakistani rupees accounted for 581 afs. Last week's exchange value of one dollar stood at 59.35 afs and 1,000 Pakistani rupees at 575afs.\n\nReferring to decline in Afghani currency, he said assistance from international community, political crisis and recent surge in insecurity, which dealt a blow to local currency.\n\n He said earlier the central bank used to inject $80 million in the market, but the amount has now declined by 50 percent.\n\nFazal Rahman, head of Food Traders' Union, said decline in the value of Afghani against foreign currency caused surge in the prices of necessary food items.\n\nAccording to his information, 50-kilogram bag of Pakistani flour accounted for 1,300 afs, 24 kilograms of rice for 1,650afs, 50 kilograms of sugar for 1,700afs, 16 litres of ghee for 1,100 afs and a kilogram of African black tea for 215afs in the outgoing week.\n\nBut a retailer in the Dahan-i-Bagh area of Kabul, Khan Ali, sold a 50-kg bag of flour for 1,450afs, a 50-kg sack of sugar for 1,800afs, a 24-kg bag of rice for 1,900afs and a 16 liter tin of Momin ghee for 1,050afs.\n\nAbdul Hadi, director of a petrol station in Wazirabad, told Pajhwok Afghan News the rate of one litre of petrol up from 44 to 45afs and the same quantity of diesel increased from 41 to 42afs\n\nA gas seller in Taimani area, Mohammad Sharif, sold one kilogram of liquefied gas for 30afs ---same as last week, but in some parts the city it has been sold on 35 afghanis.\n\nMohammad Fawad, a jeweler in the Taimur Shahi square, said due to decline in the value of Afghani currency, the price of one gram Arabic gold soared from 2,000 to 2100afs and the price of one gram Russian gold from 1,600 to 1,700 afs.\n\n- See more at: http://www.pajhwok.com/en/2015/05/21/prices-fuel-gold-kabul#sthash.D2q6YlQg.dpuf",
            "address": "Delhi,india",
            "loc": {
                "lat": 28.6139391,
                "lng": 77.2090212
            },
            "created_by": Meteor.users.find({"roles":"admin"}).fetch()[0]._id,
            "userName": "Admin",
            "image": Meteor.absoluteUrl() + "/imagesForDummyNews/kabul.jpg",
            "created_at": Math.round(Date.now() / 1000),
            "medium":"user"
        });
        News.insert({
            "title": "Decision to extend Lok Sabha session",
            "description": "Opposition on Friday attacked the government over its decision to extend the Lok Sabha session for three days and termed the move \\\"dictatorial\\\", with many members objecting to the news being reported in the media even when the House was unaware of it.\\nGovernment rejected the criticism saying the decision follows similar precedents, but acknowledged that leaders of some parties could not be informed on Thursday as the Rajya Sabha worked till late evening and many ministers, including Parliamentary Affairs Minister M Venkaiah Naidu, were busy there.\\nCongress, the Left and TMC members were most vociferous in their protests and Lok Sabha was adjourned thrice as their members trooped into the Well time and again to protest the manner in which the decision was taken.",
            "address": "odisa",
            "loc": {
                "lat": 22.4131695,
                "lng": 86.09850689999999
            },
            "created_by": Meteor.users.find({"roles":"admin"}).fetch()[0]._id,
            "userName": "Admin",
            "image": Meteor.absoluteUrl() + "/imagesForDummyNews/bsd5pxdmp2ennmex4fju.jpg",
            "created_at": Math.round(Date.now() / 1000),
            "medium":"user"
        });        
    },
    createUser: function(){
        id = Accounts.createUser({
            email: "admin@daffodilnews.com",
            password: "hrhk1234",
            profile: {
                name: "Admin"
            }
        });
        Roles.addUsersToRoles(id,'admin');        
    }
}
if(Meteor.users.find({"roles":"admin"}).count() === 0){
  Fixtures.createUser();
  Fixtures.create();  
}

