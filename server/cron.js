if (Meteor.isServer) {
    Meteor.startup(function() {
        SyncedCron.start();
    });
    SyncedCron.add({
        name: 'Adding news from the google',
        schedule: function(parser) {
            // return parser.text("every 5 seconds")
            return parser.text("every 59 mins");
            //return parser.text("at 17:40 pm");
        },
        job: function() {
            var locationForNews = ['new delhi,india', 'hongkong,china', 'mumbai,india', 'puna,india', 'ontario,canada', 'london', 'paris', 'pakistan', 'afghanistan', 'nepal,asia', 'washington', 'tokyo,japan'];
            var numbersOfNews = 2;
            for (var i = 0; i < locationForNews.length; i++) {
                var uri = "https://news.google.co.in/news?pz=1&cf=all&ned=in&hl=en&geo=" + locationForNews[i] + "&output=rss";
                uri = encodeURIComponent(uri);
                Meteor.http.get("https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=" + numbersOfNews + "&q=" + uri, {

                }, function(err, result) {
                    if (err) {
                        console.log("Error", err);
                    } else {
                        if (result && result.data && result.data.responseData && result.data.responseData.feed !== null) {
                            var location;
                            var theFeeds = result.data.responseData.feed.entries;
                            var feedTitle = result.data.responseData.feed.title;
                            var rExp = '(.*?) -'; // Command Seperated Values 1
                            var regularExpression = new RegExp(rExp, ["i"]);
                            var expression = regularExpression.exec(feedTitle);
                            if (expression != null) {
                                location = expression[1];
                            }
                            console.log(new Date() + "News for location : " + location + " news count (" + theFeeds.length + ")");
                            for (var j = 0; j < theFeeds.length; j++) {
                                var currentFeed = theFeeds[j];
                                var foundOldNews = News.find({
                                    "title": currentFeed.title
                                });
                                if (foundOldNews.fetch().length === 0) {
                                    var addressExp = currentFeed.content
                                    var fullContent = currentFeed.content;
                                    var imgSrc = fullContent.match(/src="([^"]+)"/);
                                    var dateObj = new Date(currentFeed.publishedDate);
                                    var address = location.replace(/</, "&lt;");
                                    var geo = new GeoCoder();
                                    var result = geo.geocode(address);
                                    var loc = {};
                                    if (result.length == 0) {
                                        console.log("Error");
                                    } else if (result.length != 0) {
                                        loc = {
                                            "lat": result[0].latitude,
                                            "lng": result[0].longitude
                                        }
                                    }
                                    var object = {
                                        "title": currentFeed.title,
                                        "description": currentFeed.contentSnippet,
                                        "created_at": Math.round(dateObj / 1000),
                                        "created_by": Meteor.users.find({
                                            "roles": "admin"
                                        }).fetch()[0]._id,
                                        "address": address,
                                        "loc": loc,
                                        "userName": 'Admin',
                                        "news_link": currentFeed.link,
                                        "medium": 'newsFeed'
                                    }
                                    if (imgSrc !== null && imgSrc.length > 0) {
                                        object.image = "http:" + imgSrc[1];
                                    }
                                    News.insert(object);
                                } else {
                                    console.log('News alredy exsit for ' + location + ' in system : ', currentFeed.title);
                                }                                
                            }
                        } else {
                            console.log("Error fetching feeds!");
                        }
                    }
                });
            }
        }
    });
}
