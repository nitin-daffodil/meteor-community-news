Meteor.users.deny({
  update: function() {
    return true;
  }
});

News.allow({
  update  : function(){
    return true;
  },
  remove : function(){
    return true;
  },
  insert : function(){
    return true;
  }
});