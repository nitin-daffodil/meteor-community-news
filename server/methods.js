Meteor.methods({
  saveNews: function (title, description, address, image, date) {
    if (!title) {
      throw new Meteor.Error("Title Can't be Empty");
    } else if (!description) {
      throw new Meteor.Error("Description can't be empty");
    } else if (!address) {
      throw new Meteor.Error("Address can't be Empty");
    }
    if (!date) {
      throw new Meteor.Error("Date can't be Empty");
    }
    var timestamp = Math.round(date.getTime() / 1000);
    var geo = new GeoCoder();
    var result = geo.geocode(address);
    if (result.length == 0) {
      throw new Meteor.Error("This address does't have lat & long");
    }
    var loc = {};
    if (result.length != 0) {
      loc = {
        "lat": result[0].latitude,
        "lng": result[0].longitude
      }
      var options = {};
      options = {
        "title": title,
        "description": description,
        "address": address,
        "loc": loc,
        "created_by": this.userId,
        "userName": Meteor.user().profile.name,
        "image": image,
        "created_at": timestamp,
        "medium": "user"
      }
      News.insert(options);
    }
  },
  updateNews: function (title, description, address, image, date, id) {
    var news = News.find({"_id":id});
    // console.log("news.created_by:",news.created_by , typeof news.created_by);
    // console.log("news.created_by:",Meteor.userId() , Meteor.userId());
    // if(news.created_by !== Meteor.userId()){
    //     throw new Meteor.Error("Not-Authorized"); 
    // }
    var geo = new GeoCoder();
    var result = geo.geocode(address);
    if (result.length == 0) {
      throw new Meteor.Error("This address does't have lat & long");
    }
    var loc = {};
    var timestamp = Math.round(date.getTime() / 1000);
    if (result.length != 0) {
      loc = {
        "lat": result[0].latitude,
        "lng": result[0].longitude
      }
      var options = {};
      options = {
        "title": title,
        "description": description,
        "address": address,
        "loc": loc,
        "image": image,
        "created_at": timestamp
      }
      News.update({
        "_id": id
      }, {
        $set: options
      });
    }
  },  
  deleteNews: function (id){
    // var news = News.find({"_id":id});
    // if(news.created_by !== Meteor.userId()){
    //    throw new Meteor.Error("Not-Authorized"); 
    // }
    News.remove({_id:id});
  }
})