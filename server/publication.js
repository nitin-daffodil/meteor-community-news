Meteor.publish('listOfNews', function(options) {
    var filter = {};
    var fromDate, toDate;
    var today = new Date();
    // if(!dateRange){
    // console.log("With in condition");
    if (options === 'googleNews') {
        filter = {
            "medium": "newsFeed"
        }
    } else if (options === 'otherNews') {
        filter = {
            "medium": "user"
        }
    } else if (options === 'allNews') {
        filter = {}
    } else if (options === 'today') {
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var fromDate = new Date(mm + '-' + dd + '-' + yyyy);
        var toDate = new Date();
        filter = {
            created_at: {
                $gt: Math.round(new Date(fromDate).getTime() / 1000),
                $lt: Math.round(new Date(toDate).getTime() / 1000)
            }
        };
    } else if (options === '7days') {
        fromDate = new Date(today.setDate(today.getDate() - 7));
        toDate = new Date();
        filter = {
            created_at: {
                $gt: Math.round(new Date(fromDate).getTime() / 1000),
                $lt: Math.round(new Date(toDate).getTime() / 1000)
            }
        }
    } else if (options === '30days') {
        fromDate = new Date(today.setDate(today.getDate() - 30));
        toDate = new Date();
        filter = {
            created_at: {
                $gt: Math.round(new Date(fromDate).getTime() / 1000),
                $lt: Math.round(new Date(toDate).getTime() / 1000)
            }
        }
    } else if (options) {
        filter = {
            created_by: options
        }
    }
    // } 
    // else{
    //    filter = {
    //         created_at: {
    //           $gt: Math.round(new Date(options.fromDateFromSession).getTime() / 1000),
    //           $lt: Math.round(new Date(options.toDateFromSession).getTime() / 1000)
    //         }
    //    };
    // } 
    return News.find(filter);
});

Meteor.publish('newsToBeEdit', function(newsId) {
    return News.find({
        "_id": newsId
    });
});


Meteor.publish('currentUser', function(newsId) {
    return Meteor.users.findOne(Meteor.userId())
});

Meteor.publish('newsByDateFilter', function(dateRangeGiven, days) {
    var today = new Date();
    var toDate, fromDate;
    var filter = {};
    if (!dateRangeGiven) {
        if (days.daySelected === '7days') {
            fromDate = new Date(today.setDate(today.getDate() - 7));
            toDate = new Date();
            filter = {
                created_at: {
                    $gt: Math.round(new Date(fromDate).getTime() / 1000),
                    $lt: Math.round(new Date(toDate).getTime() / 1000)
                }
            };
        } else if (days.daySelected === '30days') {
            fromDate = new Date(today.setDate(today.getDate() - 30));
            toDate = new Date();
            filter = {
                created_at: {
                    $gt: Math.round(new Date(fromDate).getTime() / 1000),
                    $lt: Math.round(new Date(toDate).getTime() / 1000)
                }
            };
        } else if (days.daySelected === 'today') {
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var fromDate = new Date(mm + '-' + dd + '-' + yyyy);
            var toDate = new Date();
            filter = {
                created_at: {
                    $gt: Math.round(new Date(fromDate).getTime() / 1000),
                    $lt: Math.round(new Date(toDate).getTime() / 1000)
                }
            };
        }
    } else {
        if (days.fromDateFromSession.getDate() === days.toDateFromSession.getDate() && days.fromDateFromSession.getMonth() == days.toDateFromSession.getMonth() && days.fromDateFromSession.getFullYear() == days.toDateFromSession.getFullYear()) {
            var dd = days.fromDateFromSession.getDate();
            var mm = days.fromDateFromSession.getMonth() + 1;
            var yyyy = days.fromDateFromSession.getFullYear();
            var fromDate = new Date(mm + '-' + dd + '-' + yyyy);
            var toDate = days.toDateFromSession;
            toDate.setHours(23, 59, 59, 999);
            filter = {
                created_at: {
                    $gt: Math.round(new Date(fromDate).getTime() / 1000),
                    $lt: Math.round(new Date(toDate).getTime() / 1000)
                }
            };
        } else {
            filter = {
                created_at: {
                    $gt: Math.round(new Date(days.fromDateFromSession).getTime() / 1000),
                    $lt: Math.round(new Date(days.toDateFromSession).getTime() / 1000)
                }
            };
        }
    }
    console.log("Filter:", filter);
    return News.find(filter);
})
